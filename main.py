import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from tqdm import tqdm


class MapLandGovUaParser:
    def __init__(self):
        self.driver = webdriver.Firefox(executable_path='./geckodriver')
        self.driver.get("https://map.land.gov.ua/")
        WebDriverWait(self.driver, 3).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="mapModal"]/div/div/div[3]/button')))
        element = self.driver.find_element_by_xpath('//*[@id="mapModal"]/div/div/div[3]/button')
        element.click()

    def find_number(self, _number: str):
        self.driver.refresh()
        element = self.driver.find_element_by_id('search_map_input')
        element.clear()
        element.send_keys(_number)
        WebDriverWait(self.driver, 3).until(EC.element_to_be_clickable((By.ID, 'search_map_input_btn')))
        element = self.driver.find_element_by_id('search_map_input_btn')
        element.click()

        WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.XPATH, '//*[@id="routeToParcel"]')))
        element = self.driver.find_element_by_xpath('//*[@id="routeToParcel"]')
        while True:
            url = element.get_attribute("data-url")
            if url:
                return url
            time.sleep(0.5)


mapLandGovUaParser = MapLandGovUaParser()
results = open('results.txt', 'a')
for number in tqdm(open('number_list.txt').read().split('\n')):
    result = mapLandGovUaParser.find_number(number)
    result = str(result).replace('https://www.google.com.ua/maps/dir//', '')
    result = str(result).split('/@')[0]
    results.write(result + '\n')
